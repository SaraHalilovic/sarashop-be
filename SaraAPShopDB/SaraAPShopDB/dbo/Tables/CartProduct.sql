﻿CREATE TABLE [dbo].[CartProduct] (
    [CartProductId] INT IDENTITY (6, 1) NOT NULL,
    [CartId]        INT NOT NULL,
    [ProductId]     INT NOT NULL,
    [Quantity]      INT NOT NULL,
    CONSTRAINT [PK_CartProduct] PRIMARY KEY CLUSTERED ([CartProductId] ASC),
    CONSTRAINT [FK_CartProduct_Cart] FOREIGN KEY ([CartId]) REFERENCES [dbo].[Cart] ([CartId]),
    CONSTRAINT [FK_CartProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([ProductId]) ON UPDATE CASCADE
);

