﻿CREATE TABLE [dbo].[ProductDetails] (
    [ProductDetailsId] INT        IDENTITY (1, 1) NOT NULL,
    [ProductId]        INT        NOT NULL,
    [DatePublished]    DATE       CONSTRAINT [DF_ProductDetails_DatePublished] DEFAULT (getdate()) NOT NULL,
    [Condition]        NCHAR (10) NOT NULL,
    [Gender]           NCHAR (10) NOT NULL,
    [Color]            NCHAR (10) NOT NULL,
    [ShippingFrom]     NCHAR (20) NOT NULL,
    [ItemsInStore]     INT        NOT NULL,
    [ItemsSold]        INT        NOT NULL,
    CONSTRAINT [PK_ProductDetails] PRIMARY KEY CLUSTERED ([ProductDetailsId] ASC),
    CONSTRAINT [FK_ProductDetails_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([ProductId])
);

