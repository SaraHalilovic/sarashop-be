﻿CREATE TABLE [dbo].[Product] (
    [ProductId]        INT           IDENTITY (1, 1) NOT NULL,
    [Code]             NCHAR (12)    NOT NULL,
    [IsActive]         INT           NOT NULL,
    [Name]             NCHAR (20)    NOT NULL,
    [Category]         NCHAR (10)    NOT NULL,
    [Image]            VARCHAR (100) NOT NULL,
    [ShortDescription] VARCHAR (100) NOT NULL,
    [Price]            FLOAT (53)    NOT NULL,
    [ShippingPrice]    FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ProductId] ASC)
);

