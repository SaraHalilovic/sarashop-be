﻿CREATE TABLE [dbo].[Order] (
    [OrderId]    INT        IDENTITY (1, 1) NOT NULL,
    [UserId]     INT        NOT NULL,
    [Date]       DATE       CONSTRAINT [DF_Orders_Date] DEFAULT (getdate()) NOT NULL,
    [TotalPrice] FLOAT (53) NOT NULL,
    CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED ([OrderId] ASC),
    CONSTRAINT [FK_Order_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);

