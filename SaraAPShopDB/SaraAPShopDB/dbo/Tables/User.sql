﻿CREATE TABLE [dbo].[User] (
    [UserId]       INT          IDENTITY (1, 1) NOT NULL,
    [UserEmail]    VARCHAR (50) NOT NULL,
    [PasswordHash] VARCHAR (50) NOT NULL,
    [Role]         NCHAR (15)   NOT NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

