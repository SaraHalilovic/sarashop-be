﻿CREATE TABLE [dbo].[Cart] (
    [CartId]          INT  IDENTITY (1, 1) NOT NULL,
    [UserId]          INT  NOT NULL,
    [DateLastUpdated] DATE CONSTRAINT [DF_Cart_DateLastUpdated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Cart] PRIMARY KEY CLUSTERED ([CartId] ASC),
    CONSTRAINT [FK_Cart_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);

