using AutoMapper;
using BusinessLogicLayer;
using DataAccessLayer;
using Filters;
using Mapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using UnitOfWork;

namespace SaraAPShop
{
    public class Startup
    {
        public readonly string MyAllowSpecificOrigins = "myPolicy";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            //DALs
            services.AddScoped<IOrderDAL, OrderDAL>();
            services.AddScoped<IUserDAL, UserDAL>();
            services.AddScoped<IProductDAL, ProductDAL>();

            //BLLs
            services.AddScoped<IProductBLL, ProductBLL>();
            services.AddScoped<IOrderBLL, OrderBLL>();
            services.AddScoped<IUserBLL, UserBLL>();


            services.AddScoped<IUnitOfWork, UoW>();

            services.AddAutoMapper(typeof(MapperProfiles));

            services.AddMvc(options =>
            {
                options.Filters.Add(new GlobalExceptionFilter());
            });

            services.AddCors(options => options.AddPolicy(MyAllowSpecificOrigins,
                 builder => builder.AllowAnyOrigin()
                     .AllowAnyHeader()
                     .AllowAnyMethod()
                 )
             );



            //var mapperConfig = new AutoMapper.MapperConfiguration(c => c.AddProfile(new Mapper.MapperProfiles()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(MyAllowSpecificOrigins);
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
          //  app.UseCors(options => options.WithOrigins("https://localhost:4200")); ///

            //app.UseCors(MyAllowSpecificOrigins);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
