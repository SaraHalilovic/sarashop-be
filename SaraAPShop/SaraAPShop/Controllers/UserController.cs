﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer;
using DataAccessLayer;
using DTOS;
using EFModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace SaraAPShop.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserDAL _userManager;
        private IOrderDAL _orderManager;
        private IUserBLL _userValidation;
        public UserController(IUserDAL userManager, IOrderDAL orderManager, IUserBLL userValidation)
        {
            _userManager = userManager;
            _orderManager = orderManager;
            _userValidation = userValidation;
        }


        [HttpGet]
        public IActionResult GetUserByUsernamePassword(string username, string password)
        {
             UserDTO user =_userValidation.ValidateLoginData(username, password); //validiraj uneseni password i id
            return Ok(user);
        }


        [HttpGet("{userId}/userEmail")]
        public ActionResult GetUserEmail(int userId)
        {
            
            return Ok(_userManager.GetUserEmail(userId));
           
        }

        [HttpGet("{userId}/OrderHistory")]
        public ActionResult<OrderDTO[]> GetOrderHistory(int userId)
        {
            
            var order_history = _orderManager.GetOrdersByUserId(userId);
           
            return Ok(order_history);
        }


        #region CART CRUD

        [HttpGet("{userId}/Cart")]
        public IActionResult LoadCartItems(int userId)
        {
            return Ok(_userManager.LoadCartItems(userId));

        }

        [HttpGet("{userId}/cart/CartItem/{cartItemId}")]
        public ActionResult GetCartItem(int userId, int cartItemId)
        {
            var result = _userManager.GetCartItem(userId, cartItemId);

            return Ok(result);
        } //sa quantity?


        [HttpPut("{userId}/cart/CartItem")]
        public ActionResult UpdateCartItem(int userId, CartItemDTO cartItem)
        {
            var result = _userManager.UpdateCartItem(userId, cartItem);

            return Ok(result);
        }

        [HttpPost ("{userId}/cart/CartItem")]
        public ActionResult AddItemToCart(int userId, CartItemDTO item)
        {
            var result = _userManager.AddToCart(item,userId);
            
            return Ok(result);
        }

        [HttpDelete("{userId}/cart/cartitem/{cartItemId}")]
        public ActionResult DeleteItemFromCart(int userId, int cartItemId)
        {
            _userManager.DeleteCartProduct(userId, cartItemId);
            return Ok();
        }

       
        #endregion

    }
}