﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer;
using DTOS;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using DataAccessLayer;
using PagingParameters;
using Newtonsoft.Json;

namespace SaraHalilovic_APShop.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductBLL _productBLL;
        private readonly IProductDAL _productmanager;
        public ProductController(IProductBLL productbll, IProductDAL productmanager)
        {
            _productBLL = productbll;
            _productmanager = productmanager;
        }
        //GetALL Products
        
        [HttpGet]
        public IActionResult GetAllProducts([FromQuery] SearchDTO query)
        {
            //ProductDTO[] 
           
            ProductPage products = _productmanager.GetAllProducts(query);
             

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(products.MetaData));

            return Ok(products.Products);
                
        }

        [HttpGet("DetailedProduct/ID/{productId}")]
        public ActionResult<object> GetProductDetails(int productId)
        {
            object detailedProduct = _productmanager.GetDetailedProduct(productId);
            return Ok(detailedProduct);
        }

        [HttpDelete("DetailedProduct/{productId}")]
        public ActionResult DeleteProduct(int productId)
        {
            //check if administrator
            _productmanager.DeleteProduct(productId);
            return Ok();
        }

        [HttpPost("DetailedProduct")]
        public ActionResult AddProduct(DetailedProductDTO product)
        {
            _productmanager.AddProduct(product);
            return Ok();
        }

        [HttpPut("DetailedProduct/{id}")]
        public ActionResult UpdateProduct(int id, DetailedProductDTO product)
        {
            var updatedProduct = _productmanager.UpdateProduct(product);
            return Ok(updatedProduct); ;
        }
    }


   
}
