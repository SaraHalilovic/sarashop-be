﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer;
using DataAccessLayer;
using DTOS;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EF = EFModels;

namespace SaraAPShop.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private IOrderDAL _orderManager;
        private IOrderBLL _orderBLL;

        public OrderController(IOrderDAL ordermanager,IOrderBLL orderBLL)
        {
            _orderManager = ordermanager;
            _orderBLL = orderBLL;
        }

        //get order details
        [HttpGet("{orderId}")]
        public ActionResult GetById(int orderId)
        {
            OrderDTO order = _orderManager.GetOrder(orderId);
             return Ok(order);
        }

        [HttpPost]
        public ActionResult MakeOrder (OrderDTO order)
        {
            _orderBLL.ValidateOrderPrice(order);
            return Ok();
        }

           
        }
}