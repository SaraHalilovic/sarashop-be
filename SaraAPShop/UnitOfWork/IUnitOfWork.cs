﻿using EFModels;
using Repositories;
using System;

namespace UnitOfWork
{
    public interface IUnitOfWork
    {
        public EshopContext Context { get; set; }
        IOrderRepository Orders { get; }
        ICartRepository Carts { get; }
        IUserRepository Users { get; }

        IProductRepository Products { get; }
        void Commit();
    }

}
