﻿using AutoMapper;
using EFModels;
using Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitOfWork
{
    public class UoW : IUnitOfWork
    {
        private EshopContext _context;
        private IMapper _mapper;

        //repositories
        private IOrderRepository _orders;
        private ICartRepository _carts;
        private IUserRepository _users;
        private IProductRepository _products;

        public EshopContext Context
        {
            get
            {
                return _context ?? throw new InvalidOperationException("Initialize Context propery before using UOW.");
            }
            set
            {
                _context = value;

                // Reset all repositories in case of new UOW Context initialization
                _orders = null;
                _users = null;
                _products = null;
                _carts = null;
            }
        }

        public UoW(IMapper mapper)
        {
            if (mapper == null)
                throw new ArgumentNullException("mapper");

            _mapper = mapper;
        }
        #region IUnitOfWork Members

        //public IRepository<User> Users
        public IUserRepository Users
        {
            get
            {
                if (_users == null)
                {
                    _users = new UserRepository(Context, _mapper);
                }
                return _users;
            }
        }
        public IProductRepository Products
        {
            get
            {
                if (_products == null)
                {
                    _products = new ProductRepository(Context);
                }
                return _products;
            }
        }

        public IOrderRepository Orders
        {
            get
            {
                if (_orders == null)
                {
                    _orders = new OrderRepository(Context);
                }
                return _orders;
            }
        }

        public ICartRepository Carts
        {
            get
            {
                if (_carts == null)
                {
                    _carts = new CartRepository(Context);
                }
                return _carts;
            }
        }





        public void Commit()
        {
            Context.SaveChanges();
        }
        #endregion
    }
}
