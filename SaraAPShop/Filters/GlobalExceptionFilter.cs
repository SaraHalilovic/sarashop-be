﻿
using Exceptions;
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Filters
{
    /// <summary>
    /// Reference: https://weblog.west-wind.com/posts/2016/oct/16/error-handling-and-exceptionfilter-dependency-injection-for-aspnet-core-apis
    /// </summary>
    public class GlobalExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            ApiError apiError = new ApiError();
            if (context.Exception is RepositoryException)
            {
                var ex = context.Exception as RepositoryException;
                context.Exception = null; 
                apiError.message = "Bad Request";
                apiError.errors = ex.Message;
                context.HttpContext.Response.StatusCode = 400;
            }
            else if (context.Exception is ValidationException)
            {
                var ex = context.Exception as ValidationException;
                context.Exception = null; 
                apiError.message = "Validation error";
                apiError.errors = ex.Message;
                context.HttpContext.Response.StatusCode = 400;
            }
            else if (context.Exception is UserException)
            {
                var ex = context.Exception as UserException;
                context.Exception = null; 
                apiError.message = "Invalid user info! ";
                apiError.errors = ex.Message;
                context.HttpContext.Response.StatusCode = 400;
            }
          
            else
            {
                apiError.message = "An unhandled error occurred.";
#if DEBUG
                apiError.errors = context.Exception.GetBaseException().Message;
                apiError.detail = context.Exception.StackTrace;
#endif
                context.HttpContext.Response.StatusCode = 500;
                
            }

            
            context.Result = new JsonResult(apiError);

            base.OnException(context);
        }
    }
}