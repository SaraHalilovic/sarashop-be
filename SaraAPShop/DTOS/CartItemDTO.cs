﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOS
{
    public class CartItemDTO
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }

        public ProductDTO Product { get; set; }

    }
}
