﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOS
{
    public class Metadata
    {
		public int CurrentPage { get; set; }
		public int TotalPages { get; set; }
		public int TotalCount { get; set; }

		public bool HasPrevious { get; set; }
		public bool HasNext { get; set; }
	}
}
