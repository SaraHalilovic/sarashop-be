﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer
{
    public class ProductPage
    {
        public ProductDTO[] Products { get; set; }
        public Metadata MetaData { get; set; }
    }
}
