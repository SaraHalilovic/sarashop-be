﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOS
{
    public class OrderDTO
    {
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }
        public double TotalPrice { get; set; }

        //public virtual User User { get; set; }
        public virtual OrderItemDTO[] OrderProduct { get; set; } //->vratiti se


    }
}
