﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOS
{
    public class UserDTO
    {
        public int UserId { get; set; }
        public string UserEmail { get; set; }
        public string PasswordHash { get; set; }
        public string Role { get; set; }

        //public virtual ICollection<Cart> Cart { get; set; }
        //public virtual ICollection<Order> Order { get; set; }
    }
}
