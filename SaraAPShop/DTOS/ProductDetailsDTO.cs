﻿using System;

namespace DTOS
{
    public class ProductDetailsDTO
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public DateTime DatePublished { get; set; }
        public string Condition { get; set; }
        public string Gender { get; set; }
        public string Color { get; set; }
        public string ShippingFrom { get; set; }
        public int ItemsInStore { get; set; }
        public int ItemsSold { get; set; }
    }
}
