﻿using System;

namespace DTOS
{
    public class ProductDTO
    {
        public int ProductId { get; set; }
        public string Code { get; set; }
        public int IsActive { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }
        public string ShortDescription { get; set; }
        public double Price { get; set; }
        public double ShippingPrice { get; set; }

    }
}
