﻿using System;

namespace DTOS
{
    public class OrderItemDTO
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }

        //public virtual Order Order { get; set; }

        public  ProductDTO Product { get; set; }
    }
}
