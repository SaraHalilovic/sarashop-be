﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOS
{
    public class DetailedProductDTO
    {
        public ProductDTO Product { get; set; }
        public ProductDetailsDTO ProductDetail { get; set; }

    }
}
