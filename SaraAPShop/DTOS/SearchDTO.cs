﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOS
{
    public class SearchDTO
    {
        public string Name { get; set; }
        public string Category { get; set; }

        public string Colour { get; set; }

        public string Gender { get; set; }
        public string Condition { get; set; }

        public double PriceFrom { get; set; }
        public double PriceTo { get; set; }
        public bool FreeShipping { get; set; }
        public string SortBy { get; set; }
        public string OrderBy { get; set; }

        public int Page { get; set; }
       
    }
}
