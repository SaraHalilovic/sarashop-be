﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer
{
    public interface IProductBLL
    {
        double CalculateTotalPrice(double price);
        
    }
}
