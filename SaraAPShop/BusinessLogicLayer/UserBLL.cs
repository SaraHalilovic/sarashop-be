﻿using DataAccessLayer;
using DTOS;
using Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer
{
    public class UserBLL : IUserBLL
    {
        private IUserDAL _userManager;
        public UserBLL(IUserDAL userManager)
        {
            _userManager = userManager;
        }
        public UserDTO ValidateLoginData(string userEmail, string password)
        {
            //provjeri da li je ista null, baci izuzetak
            if (userEmail == null || password == null) 
                throw new UserException("Username or password missing!");
            //uzmi password i hesiraj
            string hashedPassword = Hash(password);
            //dohvati username i hesiranipassword
            UserDTO user=_userManager.GetUserEmailAndPassword(userEmail, hashedPassword);

            return user;
        }

        string Hash(string pass)
        {
            int n = pass.Length-1;
            string reverse = "";

            while (n >= 0)
            {
                reverse = reverse + pass[n];
                n--;
            }
            return reverse;
        }

    }
}
