﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer
{
    public interface IOrderBLL
    {
        public bool ValidateOrderPrice(OrderDTO order);
    }
}
