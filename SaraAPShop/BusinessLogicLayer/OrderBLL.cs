﻿using AutoMapper;
using DataAccessLayer;
using DTOS;
using EFModels;
using Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using UnitOfWork;

namespace BusinessLogicLayer
{
     public class OrderBLL : IOrderBLL
    {
     
        private IOrderDAL _orderManager;

        public OrderBLL( IOrderDAL orderManager)
        {
            _orderManager = orderManager;
        }
       

        public bool ValidateOrderPrice(OrderDTO order) //ValidateOrder
        {
            


            if (order.TotalPrice != _orderManager.CalculateTotalOrderPrice(order.OrderProduct))
            {
                throw new ValidationException("Please review your order details and try again!");
            }
            
            if(_orderManager.CheckAvailability(order) == false)
            {
                throw new ValidationException("Order cannot be placed, there is no enough items in store! ");
            }
            if (_orderManager.CheckIfProductsAreActive(order) == false) 
            {
                throw new ValidationException("Some ordered items have been altered or deleted. Please refresh and review your order");
            }

            //ako je sve prošlo okej, napravi narudzbu
            _orderManager.PlaceAnOrder(order);

             return true;
                

        }

            
        
     }
}
