﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer
{
    public interface IUserBLL
    {
        public UserDTO ValidateLoginData(string userEmail, string password);
    }
}
