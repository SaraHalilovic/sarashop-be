﻿using System;
using System.Collections.Generic;

namespace EFModels
{
    public partial class Cart
    {
        public Cart()
        {
            CartProduct = new HashSet<CartProduct>();
        }

        public int CartId { get; set; }
        public int UserId { get; set; }
        public DateTime? DateLastUpdated { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<CartProduct> CartProduct { get; set; }
    }
}
