﻿using System;
using System.Collections.Generic;

namespace EFModels
{
    public partial class ProductDetails
    {
        public int ProductDetailsId { get; set; }
        public int ProductId { get; set; }
        public DateTime DatePublished { get; set; }
        public string Condition { get; set; }
        public string Gender { get; set; }
        public string Color { get; set; }
        public string ShippingFrom { get; set; }
        public int ItemsInStore { get; set; }
        public int ItemsSold { get; set; }

        public virtual Product Product { get; set; }
    }
}
