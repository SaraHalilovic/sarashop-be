﻿using System;
using System.Collections.Generic;

namespace EFModels
{
    public partial class Product
    {
        public Product()
        {
            CartProduct = new HashSet<CartProduct>();
            OrderProduct = new HashSet<OrderProduct>();
            ProductDetails = new HashSet<ProductDetails>();
        }

        public int ProductId { get; set; }
        public string Code { get; set; }
        public int IsActive { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }
        public string ShortDescription { get; set; }
        public double Price { get; set; }
        public double ShippingPrice { get; set; }

        public virtual ICollection<CartProduct> CartProduct { get; set; }
        public virtual ICollection<OrderProduct> OrderProduct { get; set; }
        public virtual ICollection<ProductDetails> ProductDetails { get; set; }
    }
}
