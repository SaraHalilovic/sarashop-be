﻿using AutoMapper;
using DTOS;
using EFModels;
using System;
using System.Collections.Generic;
using System.Text;
using UnitOfWork;

namespace DataAccessLayer
{
    public class OrderDAL : IOrderDAL
    {
        private IMapper _mapper;
        private IUnitOfWork _uow;

        public OrderDAL(IMapper mapper, IUnitOfWork uow)
        {
            _mapper = mapper;
            _uow = uow;
        }

        public OrderDTO GetOrder(int id)
        {
            using (_uow.Context = new EshopContext())
            {
                Order efOrder = _uow.Orders.GetById(id);
                OrderDTO order = _mapper.Map<OrderDTO>(efOrder);
                
                
                return order;
            }
        }

        public OrderDTO[] GetOrdersByUserId(int id)
        {
            using (_uow.Context = new EshopContext())
            {
                var ordersEF = _uow.Orders.GetOrdersByUserId(id);
                OrderDTO[] orders = _mapper.Map<OrderDTO[]>(ordersEF);
                _uow.Commit();
                return orders;
            }
        }


        public double CalculateTotalOrderPrice(OrderItemDTO[] orderProducts)
        {
            using (_uow.Context = new EshopContext())
            {
                double x =_uow.Orders.CalculateTotalOrderPrice(orderProducts);
                return (Math.Floor(x * 10d) / 10d); //popraviti proedjenje  vrijednosti tipa double
            }
        }

        public bool CheckAvailability(OrderDTO order)
        {
            using (_uow.Context = new EshopContext())
            {
                
                return (_uow.Products.CheckAvailability(order)); 
            }
        }

        public bool CheckIfProductsAreActive(OrderDTO order)
        {
            using (_uow.Context = new EshopContext())
            {
                return _uow.Products.CheckIfProductsAreActive(order);
            }
        }
        public void PlaceAnOrder(OrderDTO order)
        {
            using (_uow.Context = new EshopContext())
            {
                
                var a = _mapper.Map<Order>(order);
               

                
                _uow.Orders.PlaceAnOrder(a); 
                //sad treba obriisati iz carta
                foreach(OrderItemDTO orderItem in order.OrderProduct)
                {
                    _uow.Carts.RemoveItemFromCart(order.UserId, orderItem.ProductId);
                }
                //ovdje treba otici i svaki umanjiti u baziii
                _uow.Products.UpdateProductsInStore(order);
                _uow.Commit();
            }
        }

    }
}
