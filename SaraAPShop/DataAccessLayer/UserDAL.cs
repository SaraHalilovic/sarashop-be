﻿using AutoMapper;
using DTOS;
using EF = EFModels;
using System;
using System.Collections.Generic;
using System.Text;
using UnitOfWork;
using EFModels;
using Exceptions;

namespace DataAccessLayer
{
    public class UserDAL : IUserDAL
    {
        private IMapper _mapper;
        private IUnitOfWork _uow;

        public UserDAL(IMapper mapper, IUnitOfWork uow)
        {
            _mapper = mapper;
            _uow = uow;
        }
       

        public object GetUserEmail(int userId)
        {
            using (_uow.Context = new EshopContext())
            {
                string userEmail = _uow.Users.GetEmailByUserId(userId);
                var email = new
                {
                    username = userEmail
                };
                return email;

            }
        }

        public UserDTO GetUserEmailAndPassword(string userEmail, string hashedPassword)
        {
            using(_uow.Context = new EshopContext())
            {
                User user = _uow.Users.GetUserCredentials(userEmail, hashedPassword);
                if (user == null)
                    throw new UserException("Invalid credentials!");
                _uow.Commit();
                return (_mapper.Map<UserDTO>(user));
            }
        }
        #region CART
        public object LoadCartItems(int userId)
        {
            using (_uow.Context = new EshopContext())
            {
               var cartproducts = _uow.Carts.GetCartItems(userId);
               CartItemDTO[] products = _mapper.Map<CartItemDTO[]>(cartproducts);
                _uow.Commit();
               return products;
            }
        }

        public object AddToCart(CartItemDTO item, int userId)
        {
            using (_uow.Context = new EshopContext())
            {

                var result = _uow.Carts.AddItemToCart(item, _uow.Carts.GetById(userId).CartId);
                //CartItemDTO addedItem = _mapper.Map<CartItemDTO>(result);

                _uow.Commit();
                 return result;
            }
        }


        public void DeleteCartProduct(int userId, int productId)
        {
            using(_uow.Context = new EshopContext())
            {
                _uow.Carts.RemoveItemFromCart(userId,productId);
                _uow.Commit();
            }
        }


        public DetailedProductDTO GetCartItem(int userId, int cartItemId)
        {
            using (_uow.Context = new EshopContext())
            {
                DetailedProductDTO productWithDetails = new DetailedProductDTO();

                var productWithDetailsEF = _uow.Carts.GetItemFromCart(userId, cartItemId);
               
                if (productWithDetailsEF != null)
                {
                    productWithDetails.Product = _mapper.Map<ProductDTO>(productWithDetailsEF.Product);
                    productWithDetails.ProductDetail = _mapper.Map<ProductDetailsDTO>(productWithDetailsEF);
                }

                _uow.Commit();
                return productWithDetails;
            }
        }

        public CartItemDTO UpdateCartItem(int userId, CartItemDTO cartItem)
        {
            using (_uow.Context = new EshopContext())
            {
                var result =_uow.Carts.UpdateCartItem(userId, cartItem);
                CartItemDTO updatedproduct = _mapper.Map<CartItemDTO>(result);
                _uow.Commit();
                return updatedproduct;
            }
        }

        #endregion

    }
}
