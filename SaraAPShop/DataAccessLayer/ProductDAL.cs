﻿using AutoMapper;
using DTOS;
using EFModels;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PaginationLogic;
using PagingParameters;
using System;
using System.Collections.Generic;
using System.Text;
using UnitOfWork;

namespace DataAccessLayer
{
    public class ProductDAL : IProductDAL
    {
        private IMapper _mapper;
        private IUnitOfWork _uow;

        public ProductDAL(IMapper mapper, IUnitOfWork uow)
        {
            _mapper = mapper;
            _uow = uow;
        }


        public ProductPage GetAllProducts(SearchDTO query)
        {
            if (query.Page == 0) 
                    query.Page = 1;
           
            using (_uow.Context = new EshopContext())
            {
                var efProducts = _uow.Products.GetAllProducts(query);
                ProductDTO[] products = _mapper.Map<ProductDTO[]>(efProducts);
                 
                //Ovo izdvojiti u metodu
                ProductPage pageInfo = new ProductPage();
                Metadata headerData = new Metadata();
                
                headerData.CurrentPage = efProducts.CurrentPage;
                headerData.HasNext = efProducts.HasNext;
                headerData.HasPrevious = efProducts.HasPrevious;
                headerData.TotalCount = efProducts.TotalCount;
                headerData.TotalPages = efProducts.TotalPages;
                
                pageInfo.Products = products;
                pageInfo.MetaData = headerData;



                return pageInfo;
            }

        }

       

        public DetailedProductDTO GetDetailedProduct(int productId)
        {
            using (_uow.Context = new EshopContext())
            {
                DetailedProductDTO productWithDetails = new DetailedProductDTO();

                ProductDetails productWithDetailsEF = _uow.Products.GetProductDetailsByID(productId);

                if (productWithDetailsEF != null)
                {
                    productWithDetails.Product = _mapper.Map<ProductDTO>(productWithDetailsEF.Product);
                    productWithDetails.ProductDetail = _mapper.Map<ProductDetailsDTO>(productWithDetailsEF);
                }

                _uow.Commit();
                return productWithDetails;

              
            }
        }

        public void DeleteProduct(int productId)
        {
            using (_uow.Context = new EshopContext())
            {
                _uow.Products.DeleteProduct(productId);
                _uow.Carts.UpdateCart("delete", productId, -2);
                _uow.Commit();
            }
        }

        public void AddProduct(DetailedProductDTO product)
        {
            using (_uow.Context = new EshopContext()) 
            {
                var newProduct = _mapper.Map<ProductDetails>(product.ProductDetail);
                newProduct.Product = _mapper.Map<Product>(product.Product);
                _uow.Products.AddProduct(newProduct);
                _uow.Commit();
            }
            
        }

        public DetailedProductDTO UpdateProduct(DetailedProductDTO product)
        {
            using (_uow.Context = new EshopContext())
            {
                int previousId = product.Product.ProductId;
                _uow.Products.DeleteProduct(previousId); //setting isActive = false for previous version of product

                /*   ProductDetails novi = new ProductDetails();
                   novi = _mapper.Map<ProductDetails>(product.ProductDetail);
                   novi.Product = _mapper.Map<Product>(product.Product);*/
                // novi.Product.ProductId = 0;
                //vratiti na prethodnu verziju, proradilo
                ProductDetails noviskroz = new ProductDetails();
                noviskroz.ItemsInStore = product.ProductDetail.ItemsInStore;
                noviskroz.ItemsSold = product.ProductDetail.ItemsSold;
                noviskroz.ShippingFrom= product.ProductDetail.ShippingFrom;
                noviskroz.Color = product.ProductDetail.Color;
                noviskroz.Condition = product.ProductDetail.Condition;
                noviskroz.Gender = product.ProductDetail.Gender;
                noviskroz.Product = new Product
                {
                    Code = product.Product.Code,
                    Category = product.Product.Category,
                    IsActive = product.Product.IsActive,
                    Image = product.Product.Image,
                    Name = product.Product.Name,
                    Price = product.Product.Price,
                    ShippingPrice = product.Product.ShippingPrice,
                    ShortDescription = product.Product.ShortDescription

                };

                int newProductId = _uow.Products.UpdateProduct(noviskroz);
                product.Product.ProductId = newProductId;
                //prodji kroz cart i gdje je god bio stari id zamijeni sa novim id-em ?
                 _uow.Commit();
                _uow.Carts.UpdateCart("update", previousId, newProductId);
                _uow.Commit();
                return product;
            }

            }
        Object CreateObject(PagedList<Product> efProduct, ProductDTO[] products)
        {
            var e = new
            {
                TotalPages = efProduct.TotalPages,
                HasPrevious = efProduct.HasPrevious,
                HasNext = efProduct.HasNext,
                CurrentPage = efProduct.CurrentPage

            };

            var listOfProducts = new
            {
                products = products,
                header = e

            };

            return listOfProducts;

        } //srediti
    }
}
