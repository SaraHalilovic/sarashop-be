﻿using DTOS;
using PagingParameters;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer
{
    public interface IProductDAL
    {

        public ProductPage GetAllProducts(SearchDTO querys);
       // public ProductDTO[] FilterProducts(SearchDTO search);
        public DetailedProductDTO GetDetailedProduct(int productId);
        public void DeleteProduct(int productId);
        public void AddProduct(DetailedProductDTO product);
        public DetailedProductDTO UpdateProduct(DetailedProductDTO product);
    }
}
