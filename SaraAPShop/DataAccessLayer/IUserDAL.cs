﻿using DTOS;
using EFModels;
using System;
using System.Collections.Generic;

namespace DataAccessLayer
{
    public interface IUserDAL
    {
        
        public object GetUserEmail(int userId);
        public UserDTO GetUserEmailAndPassword(string userEmail, string hashedPassword);
        public object LoadCartItems(int userId);
        public object AddToCart(CartItemDTO item, int userId);
        public void DeleteCartProduct(int userId, int productId);
        public DetailedProductDTO GetCartItem(int userId, int cartItemId);
        public CartItemDTO UpdateCartItem(int userId, CartItemDTO cartItem);
    }
}
