﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Text;
using EF = EFModels;

namespace DataAccessLayer
{
    public interface IOrderDAL
    {
        public OrderDTO GetOrder(int id);
        public OrderDTO[] GetOrdersByUserId(int id);
        public void PlaceAnOrder(OrderDTO order);
        public double CalculateTotalOrderPrice(OrderItemDTO[] orderProducts);
        public bool CheckAvailability(OrderDTO order);
        public bool CheckIfProductsAreActive(OrderDTO order);


    }
}
