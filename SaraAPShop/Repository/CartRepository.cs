﻿using DTOS;
using EFModels;
using Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories
{
    public class CartRepository : Repository<Cart>, ICartRepository
    {
             private EshopContext _context;

        public CartRepository(EshopContext context)
                : base(context)
        {
            _context = context;
        }

        public override Cart GetById(object id)
        {
            var carts = _dbSet.Where(o => o.UserId == (int)id).AsNoTracking().SingleOrDefault();

            if(carts == null)
            {
                carts = new Cart() { UserId = (int)id };
                _context.Cart.Add(carts);
              //  _context.SaveChanges();
            }
             return carts;
        }
        public ProductDetails GetItemFromCart(int userId, int cartItemId)
        {
            int cartId = GetById(userId).CartId;
            
            if (!ItemExists(cartItemId, cartId))
            {
                throw new RepositoryException("Selected item is not in the cart");
            }
             var product = _context.ProductDetails.Include("Product")
                                .FirstOrDefault(u => u.ProductId == cartItemId && u.Product.IsActive == 1);
            
            return product;
        }

        public CartProduct[] GetCartItems(int userId)
        {  
            int cartId = GetById(userId).CartId;
            var cartProducts = _context.CartProduct.Include(a => a.Product).Where(c => c.CartId == cartId && c.Product.IsActive == 1).ToArray();

            return cartProducts;
        }

        public object AddItemToCart(CartItemDTO item, int cartId) //void
        {
            if (ItemExists(item.ProductId,cartId))
            {
                throw new RepositoryException("Item is already added to the cart! ");
                
            }
                CartProduct  cartItem= new CartProduct() { CartId = cartId, ProductId = item.ProductId, Quantity=item.Quantity };
                _context.CartProduct.Add(cartItem);
               

            return true;
        }

       public void RemoveItemFromCart(int userId, int productId)
        {
            int cartId = GetById(userId).CartId;
           
            if (ItemExists(productId, cartId) == false)
            {
                throw new RepositoryException("Item is not in  the cart! ");
                
            }

            //vidjeti bez tracking
            CartProduct e = _context.CartProduct.Where(c => cartId == c.CartId && productId == c.ProductId).FirstOrDefault();
            if(e != null)
            _context.CartProduct.Remove(e);
        }

        

        public CartProduct UpdateCartItem(int userId, CartItemDTO cartItem)
        {
            int cartId = GetById(userId).CartId;
            if (!ItemExists(cartItem.ProductId, cartId))
            {
                throw new RepositoryException("Selected item is not in the cart");
            }
            var result = _context.CartProduct.Where(x => x.ProductId == cartItem.ProductId && x.CartId == cartId).SingleOrDefault(); 
            
            
            result.Quantity = cartItem.Quantity; 
          

            return result;
        }

        public void UpdateCart (string action, int previusProductId, int newProductId)
        {
            var cartItems = _context.CartProduct.Where(x => x.ProductId == previusProductId).ToArray();

            if (action == "delete")
             {
                //sad za svaki proci i obrisati
                foreach (CartProduct x in cartItems)
                {
                    _context.CartProduct.Remove(x);
                    
                }
             }
             else if (action == "update")
            {
                foreach (CartProduct x in cartItems)
                {
                    x.ProductId = newProductId;

                }
            }
        }

        bool ItemExists(int itemId, int cartId)
        {
            var i = _context.CartProduct.Where(c => c.CartId == cartId && c.ProductId == itemId).AsNoTracking().ToArray(); //treba includati 
            if (i.Length > 0) return true;
            return false;
        }

        int getCartProductId(CartProduct cartProduct)
        {
            int a = (_context.CartProduct.Where(c => c.ProductId == cartProduct.ProductId && c.CartId == cartProduct.CartId).AsNoTracking()
                                           .SingleOrDefault()).CartProductId;
            return a;
        }


    }
}
