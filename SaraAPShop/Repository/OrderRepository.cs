﻿using DTOS;
using EFModels;
using Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
        {
             private EshopContext _context;

        public OrderRepository(EshopContext context)
                : base(context)
            {
                _context = context;
            }

            public override Order GetById(object id)
            {
            
                var order = _dbSet.Where(o => o.OrderId == (int)id).Include(a => a.OrderProduct)
                                     .ThenInclude(p => p.Product)
                                     .ToList().First();
            return order;
            }

        public Order[] GetOrdersByUserId(int userId)
        {
            var orders = _context.Order.Where(o => o.UserId == userId).Include(b => b.OrderProduct)
                         .OrderByDescending(x => x.Date)
                         .ToArray();
            return orders;

        }

        public double CalculateTotalOrderPrice(OrderItemDTO[] orderProducts)
        {
            double total = 0;
            for(int i =0; i < orderProducts.Length; i++)
            {
                var product = _context.Product.Where(x => x.ProductId == orderProducts[i].ProductId).SingleOrDefault();
                total += product.Price * orderProducts[i].Quantity + product.ShippingPrice;
            }

            return total;
        }

        //>Amar rijesio misteriju ^^, prepraviti
        //Add dodaje duple redove u product i orderitem tabeli 
        //privremeno rješenje
        public void PlaceAnOrder(Order order)
        {
            _dbSet.Attach(order);
        }

    }
   }

