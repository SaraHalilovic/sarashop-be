﻿using EFModels;
using Microsoft.EntityFrameworkCore;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Repositories
{
    public abstract class Repository<T> : IRepository<T>
       where T : class
    {
        //protected IObjectSet<T> _objectSet;
        protected DbSet<T> _dbSet;
        public Repository(EshopContext context)
        {
            //_objectSet = context.CreateObjectSet<T>();
            _dbSet = context.Set<T>();
        }
        #region IRepository<T> Members
        public abstract T GetById(object id);
        public IEnumerable<T> GetAll()
        {
            //return _objectSet;
            return _dbSet.ToArray();
        }
        public IEnumerable<T> Query(Expression<Func<T, bool>> filter)
        {
            //return _objectSet.Where(filter);
            return _dbSet.Where(filter).ToArray();
        }
        public void Add(T entity)
        {
            //_objectSet.AddObject(entity);
            _dbSet.Add(entity);
        }
        public void Remove(T entity)
        {
            //_objectSet.DeleteObject(entity);
            _dbSet.Remove(entity);
        }
        #endregion
    }
}
