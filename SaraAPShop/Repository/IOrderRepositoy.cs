﻿using DTOS;
using EFModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories
{
    public interface IOrderRepository
    {
        public void PlaceAnOrder(Order order);
        public Order GetById(object id);
        public double CalculateTotalOrderPrice(OrderItemDTO[] orderProducts);
        Order[] GetOrdersByUserId(int userId);

    }
}