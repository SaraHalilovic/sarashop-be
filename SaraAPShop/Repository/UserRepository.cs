﻿using AutoMapper;
using EFModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private EshopContext _context;
        public UserRepository(EshopContext context, IMapper mapper)
            : base(context)
        {
            _context = context;
        }

       
        

        public override User GetById(object id)
        {
            int _id = (int)id;
            return _dbSet.SingleOrDefault(u => u.UserId == _id);
        }

        public string GetEmailByUserId (int userId)
        {
            var userEF = _dbSet.SingleOrDefault(u => u.UserId == userId);
            return userEF.UserEmail;
        }

        public User GetUserCredentials(string userEmail, string hashedPassword)
        {
            var user = _dbSet.SingleOrDefault(u => u.UserEmail.Equals(userEmail) && u.PasswordHash.Equals(hashedPassword));
            return user;
        }
        private bool DoesUserExist(int userId)
        {
            if (_dbSet.Single(u => u.UserId == userId) == null)
                return false;

            return true;
        }

       
    }
}
