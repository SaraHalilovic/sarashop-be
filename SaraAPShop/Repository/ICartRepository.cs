﻿using DTOS;
using EFModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories
{
    public interface ICartRepository
    {
        public CartProduct[] GetCartItems(int userId);
        public Cart GetById(object id);
        public object AddItemToCart(CartItemDTO item, int userId);

        public void RemoveItemFromCart(int userId, int productId);

       public ProductDetails GetItemFromCart(int userId, int cartItemId);

        public CartProduct UpdateCartItem(int userId, CartItemDTO cartItem);
        public void UpdateCart(string action, int previusProductId, int newProductId);

    }
}
