﻿using DTOS;
using EFModels;
using PaginationLogic;
using PagingParameters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories
{
    public interface IProductRepository
    {
        public PagedList<Product> GetAllProducts(SearchDTO query);
        public ProductDetails GetProductDetailsByID(int _productId);
        //   public Product[] GetFilteredProducts(SearchDTO search);
        public void DeleteProduct(int productId);
        public void AddProduct(ProductDetails product);
        public int UpdateProduct(ProductDetails previousVersion);
        public Product GetById(object id);
        public bool CheckAvailability(OrderDTO order);
        public bool CheckIfProductsAreActive(OrderDTO order);
        public void UpdateProductsInStore(OrderDTO order);
    }
}
