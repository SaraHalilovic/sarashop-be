﻿using AutoMapper;
using DTOS;
using EFModels;
using Microsoft.EntityFrameworkCore;
using PaginationLogic;
using PagingParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories
{
    public class ProductRepository: Repository<Product>, IProductRepository

    {
        private EshopContext _context;
       // private ProductParameters pages = new ProductParameters();
        private int page_size = 10;
        
        public ProductRepository(EshopContext context)
            : base(context)
        {
            _context = context;
        }
        public override Product GetById(object id)
        {
            int _id = (int)id;
             return _dbSet. SingleOrDefault(u => u.ProductId == _id);
        }

        public PagedList<Product> GetAllProducts(SearchDTO search) 
        {
            var products = _context.ProductDetails.Include("Product")
                           .Where(u => u.Product.IsActive == 1);
            if (products != null)
            {
                if (search.Name != null)
                    products = products.Where(u => u.Product.Name.Contains(search.Name));


                if (search.Category != null)
                    products = products.Where(u => u.Product.Category == search.Category);

                if (search.Condition != null)
                    products = products.Where(u => u.Condition == search.Condition);

                if (search.Colour != null)
                    products = products.Where(u => u.Color == search.Colour);

                if (search.PriceFrom != 0)
                    products = products.Where(u => u.Product.Price >= search.PriceFrom);

                if (search.PriceTo != 0)
                    products = products.Where(u => u.Product.Price <= search.PriceTo);

                if (search.FreeShipping == true)
                    products = products.Where(u => u.Product.ShippingPrice == 0);

                if (search.Gender != null)
                    products = products.Where(u => u.Gender == search.Gender || u.Gender=="Unisex");

            }

            var filteredProducts = products.Select(c => new Product
            {
                ProductId = c.Product.ProductId,
                Code = c.Product.Code,
                IsActive = c.Product.IsActive,
                Name = c.Product.Name,
                Category = c.Product.Category,
                Image = c.Product.Image,
                ShortDescription = c.Product.ShortDescription,
                Price = c.Product.Price,
                ShippingPrice = c.Product.ShippingPrice

            });
            var result = filteredProducts;
            
            int orderby = 1;
            if (search.OrderBy == "DESC") orderby = -1;

            if (search.OrderBy == "DESC" && search.SortBy == "Alphabet")
            {
                result = filteredProducts.OrderByDescending(x => x.Name);
            }
            else
            {

                switch (search.SortBy)
                {
                    case "Price":
                        result = filteredProducts.OrderBy(x => x.Price * orderby);
                        break;
                    case "Shipping":
                        result = filteredProducts.OrderBy(x => (x.Price + x.ShippingPrice) * orderby);
                        break;
                    default:
                        if (search.SortBy == "Alphabet") result = filteredProducts.OrderBy(x => x.Name);
                        else
                        {

                            result = filteredProducts.OrderByDescending(x => x.ProductId);
                        }
                        break;
                }
            }

            
            return PagedList<Product>.ToPagedList(result, //---!!!!!
                   search.Page,
                   page_size);
        }
        public ProductDetails GetProductDetailsByID(int _productId)
        {
            var product = _context.ProductDetails.Include("Product")
                            .FirstOrDefault(u => u.ProductId == _productId && u.Product.IsActive == 1);
            return product;
        }

        public void DeleteProduct( int productId)
        {
            var product = _dbSet.Where(x => x.ProductId == productId).Single();
            product.IsActive = 0;
        }

        public void AddProduct(ProductDetails product)
        {
            _context.ProductDetails.Add(product);
        }

        public int UpdateProduct(ProductDetails newVersion)
        {
             _context.ProductDetails.Add(newVersion);
            return newVersion.ProductId;
        }

        public bool CheckAvailability(OrderDTO order)
        {
            //PROCI kroz svaki item i check stanje u bazi
            foreach(OrderItemDTO product in order.OrderProduct)
            {
                var _productDetails = _context.ProductDetails.Include(p => p.Product).Where(x => x.ProductId == product.ProductId).Single();
                if (_productDetails.ItemsInStore < product.Quantity)
                    return false;
            }
            return true;
        }

        public bool CheckIfProductsAreActive(OrderDTO order)
        {
            foreach(OrderItemDTO product in order.OrderProduct)
            {
                var _product = _dbSet.Where(p => p.ProductId == product.ProductId).Single();
                if (_product.IsActive == 0)
                    return false;

            }

            return true;
        }

        public void UpdateProductsInStore(OrderDTO order)
        {
            foreach(OrderItemDTO product in order.OrderProduct)
            {
                var updateProduct = _context.ProductDetails.Where(p => p.ProductId == product.ProductId).Single();
                updateProduct.ItemsInStore -= product.Quantity;
                updateProduct.ItemsSold += product.Quantity;
            }
        }
    }
}


    