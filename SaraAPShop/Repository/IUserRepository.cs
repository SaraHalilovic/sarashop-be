﻿using EFModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        
       // IEnumerable<CartProduct> GetCartProductsByUserId(int userId);
        public string GetEmailByUserId(int userId);
        public User GetUserCredentials(string userEmail, string hashedPassword);
    }
}
