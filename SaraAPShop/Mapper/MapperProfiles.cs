﻿using AutoMapper;
using DTOS;
using System;
using EF = EFModels;

namespace Mapper
{
    public class MapperProfiles : Profile
    {
        public MapperProfiles()
        {
            CreateMap<EF.User, UserDTO>().ReverseMap();

            CreateMap<EF.Cart, CartDTO>().ReverseMap();
            CreateMap<EF.OrderProduct, OrderItemDTO>().ReverseMap();
            CreateMap<EF.CartProduct, CartItemDTO>().ReverseMap();


            CreateMap<EF.Order, OrderDTO>()
             // .ForMember(dest => dest.OrderProduct, opt => opt.MapFrom(src => src.OrderProduct))
                .ReverseMap();

            //Order orderproduct u orderitem
            CreateMap<EF.OrderProduct, OrderItemDTO>().ReverseMap();
               

            CreateMap<EF.CartProduct, OrderItemDTO>().ReverseMap();

            CreateMap<EF.CartProduct, CartItemDTO>().ReverseMap();

            CreateMap<EF.CartProduct, EF.CartProduct>()
                .ForMember(dest => dest.CartProductId, opt => opt.Ignore())
                .ForMember(dest => dest.CartId, opt => opt.Ignore());

            CreateMap<EF.OrderProduct, OrderItemDTO>().ReverseMap(); //ovoooo

            CreateMap<EF.Product, ProductDTO>().ReverseMap();

            CreateMap<EF.ProductDetails, ProductDetailsDTO>().ReverseMap();

          
        }
    }
}
